# Services 
The deploy scripts allow us to choose between a multitude of services to supplement the install of Matrix. 
This page lists which ones should be changed from their default values, taken from upsteam's README. 

## Postgres
By default, the playbooks install Postgres inside a container along with everything else. Running production databases inside of containers doesn't sound like a good idea.

While a quick Google search informs me that this may not longer be the case, I'd still really rather install the database on a dedicated server, or on the host itself. The upsteam docs claim that installing Postgres on the host itself [will not work](https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/configuring-playbook-external-postgres.md), but I find this hard to believe.  

## Letsencrypt 
I'm gonna disable this in the dev version and use my CA, but we should probably turn this back on for prod.
## mx-puppet-discord
Enable this, a bridge may be helpful. This seems to be the better one, since it supports DMs. 