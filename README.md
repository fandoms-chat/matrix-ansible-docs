# Matrix Ansible docs

This documentation details how to use the [Matrix Ansible deploy scripts](https://github.com/spantaleev/matrix-docker-ansible-deploy) to deploy an instance of the Valhalla backup server. 
These docs are considered the DR plan should Discord ever turn evil and force us off of their service, or fold.

## Contents 
* [Components](docs/components.md)
* [Config](docs/config.md)